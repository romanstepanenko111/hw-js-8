// 1
let paragraphs = document.getElementsByTagName('p');


for (let i = 0; i < paragraphs.length; i++) {
   paragraphs[i].style.backgroundColor = '#ff0000';
}

// 2

let optional = document.getElementById('optionsList');
console.log(optional.parentNode);
console.log(optional.childNodes);

for (let i = 0; i < optional.childNodes.length; i++) {
    console.log(optional.childNodes[i].nodeName + ' ' + typeof optional.childNodes[i]);
    
}

// 3

let content = document.getElementById('testParagraph');
content.innerHTML = 'This is a paragraph';



// 4 

let nestedEl = document.querySelector('.main-header').children;
console.log(nestedEl);

for (let i = 0; i < nestedEl.length; i++) {
    console.log(nestedEl[i]);
    nestedEl[i].classList.add('nav-item');
    
}
// 5

let removeClass = document.querySelectorAll('.section-title')
console.log(removeClass);


for (let i = 0;  i < removeClass.length; i++) {
    // console.log(removeClass[i]);
    removeClass[i].classList.remove("section-title");
    console.log(removeClass[i].classList);
    
}